import React from "react";
import Navbar from "./NavBar";
import Head from "next/head";

function Layout(props) {
  return (
    <div>
      <Head>
        <title>BitzPrice</title>
        <link
          rel="stylesheet"
          href=" https://bootswatch.com/4/cerulean/bootstrap.min.css"
        ></link>
      </Head>
      <Navbar />
      <div className="container">{props.children}</div>
    </div>
  );
}

export default Layout;
