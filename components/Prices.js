import React, { useState } from "react";

function Prices(props) {
  const [currency, setCurrency] = useState("EUR");
  let listItems;
  if (currency === "USD") {
    listItems = (
      <li className="list-group-item">
        Bitcoin rate for {props.bpi.bpi.USD.description}:{" "}
        <span className="badge badge-primary">{props.bpi.bpi.USD.code}</span>
        <strong>{props.bpi.bpi.USD.rate}</strong>
      </li>
    );
  } else if (currency === "GBP") {
    listItems = (
      <li className="list-group-item">
        Bitcoin rate for {props.bpi.bpi.GBP.description}:{" "}
        <span className="badge badge-primary">{props.bpi.bpi.GBP.code}</span>
        <strong>{props.bpi.bpi.GBP.rate}</strong>
      </li>
    );
  } else if (currency === "EUR") {
    listItems = (
      <li className="list-group-item">
        Bitcoin rate for {props.bpi.bpi.EUR.description}:{" "}
        <span className="badge badge-primary">{props.bpi.bpi.EUR.code}</span>
        <strong>{props.bpi.bpi.EUR.rate}</strong>
      </li>
    );
  }
  return (
    <div>
      <ul className="list-group" style={{ marginBottom: "20px" }}>
        {listItems}
      </ul>
      <select
        className="form-control"
        onChange={(e) => {
          setCurrency(event.target.value);
        }}
      >
        <option value="USD">USD</option>
        <option value="GBP">GBP</option>
        <option value="EUR">EUR</option>
      </select>
    </div>
  );
}

export default Prices;
