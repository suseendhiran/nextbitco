import React from "react";
import Fetch from "isomorphic-unfetch";
import Layout from "../components/Layout";
import Prices from "../components/Prices";

function index(props) {
  return (
    <Layout>
      <h1>Welcome to BitzPrice</h1>

      <Prices bpi={props.bpi} />
    </Layout>
  );
}

index.getInitialProps = async function () {
  const repsonse = await fetch(
    "https://api.coindesk.com/v1/bpi/currentprice.json"
  );
  const data = await repsonse.json();
  console.log(data);

  return {
    bpi: data,
  };
};

export default index;
